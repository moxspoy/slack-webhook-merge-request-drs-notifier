// index.js

const https = require('https');

module.exports = async (req, res) => {
    const requestBody = req.body;

    // Check if the merge request title contains "DRS"
    if (requestBody.object_attributes.title.includes('DRS')) {
        // Compose your Slack message
        const slackMessage = {
            text: `New merge request with "DRS" in the title: ${requestBody.object_attributes.title}`,
        };

        // Send the message to Slack
        await postToSlack(slackMessage);

        res.status(200).send('Notification sent to Slack');
    } else {
        res.status(200).send('No action needed');
    }
};

function postToSlack(message) {
    const slackWebhookUrl = 'https://hooks.slack.com/services/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';

    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const req = https.request(slackWebhookUrl, options, (res) => {
        res.setEncoding('utf8');
        res.on('data', () => {});
    });

    req.on('error', (e) => {
        console.error(`Error sending message to Slack: ${e.message}`);
    });

    req.write(JSON.stringify(message));
    req.end();
}
